<?php

use App\Http\Controllers\ClubController;
use App\Http\Controllers\MatchScoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::apiResource('club', ClubController::class);
Route::apiResource('match-score', MatchScoreController::class);
Route::get('/standing', [MatchScoreController::class, 'standing']);
