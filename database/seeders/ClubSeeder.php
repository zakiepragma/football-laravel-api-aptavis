<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $clubs = [
            ['name' => 'Chelsea', 'city' => 'London'],
            ['name' => 'Liverpool', 'city' => 'Liverpool'],
            ['name' => 'Barcelona', 'city' => 'Barcelona'],
            ['name' => 'AC Milan', 'city' => 'Milan'],
            ['name' => 'Manchester United', 'city' => 'Manchester'],
            ['name' => 'Manchester City', 'city' => 'Manchester'],
            ['name' => 'Real Madrid', 'city' => 'Madrid'],
            ['name' => 'Bayern Munchen', 'city' => 'Munich'],
            ['name' => 'Juventus', 'city' => 'Turin'],
            ['name' => 'Inter Milan', 'city' => 'Milan'],
            ['name' => 'AS Roma', 'city' => 'Roma'],
            ['name' => 'Arsenal', 'city' => 'London'],
            ['name' => 'Tottenham', 'city' => 'London'],
            ['name' => 'Napoli', 'city' => 'Napoli'],
            ['name' => 'PSG', 'city' => 'Paris'],
        ];

        DB::table('clubs')->insert($clubs);
    }
}
