<?php

namespace App\Http\Controllers;

use App\Models\Club;
use App\Models\MatchScore;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class MatchScoreController extends Controller
{
    public function index()
    {
        $matchScores = MatchScore::all();
        return response()->json($matchScores);
    }

    public function show($id)
    {
        $matchScore = MatchScore::findOrFail($id);
        return response()->json($matchScore);
    }

    public function store(Request $request)
    {
        // Validasi data input
        $validator = Validator::make($request->all(), [
            'home_team_id' => 'required|exists:clubs,id',
            'away_team_id' => 'required|exists:clubs,id',
            'home_team_score' => 'required|integer',
            'away_team_score' => 'required|integer',
            // Validasi home_team_id dan away_team_id tidak boleh sama
            'away_team_id' => 'different:home_team_id',
        ]);

        // Jika validasi gagal, kirim response error
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], Response::HTTP_BAD_REQUEST);
        }

        // Cek apakah data pertandingan sudah ada di database atau belum
        $existingMatchScore = MatchScore::where([
            ['home_team_id', $request->home_team_id],
            ['away_team_id', $request->away_team_id],
        ])->first();

        // Jika data pertandingan sudah ada di database, kirim response error
        if ($existingMatchScore) {
            return response()->json([
                'error' => 'Match score already exists in database',
            ], Response::HTTP_BAD_REQUEST);
        }

        // Jika data pertandingan belum ada di database, simpan ke database
        $matchScore = new MatchScore();
        $matchScore->home_team_id = $request->home_team_id;
        $matchScore->away_team_id = $request->away_team_id;
        $matchScore->home_team_score = $request->home_team_score;
        $matchScore->away_team_score = $request->away_team_score;
        $matchScore->save();

        // Kirim response dengan status code 201 (Created) dan data pertandingan yang telah disimpan
        return response()->json([
            'message' => 'Match score created successfully',
            'data' => $matchScore,
        ], Response::HTTP_CREATED);
    }

    public function update(Request $request, $id)
    {
        $matchScore = MatchScore::findOrFail($id);

        $validatedData = $request->validate([
            'home_team_id' => 'required|exists:clubs,id',
            'away_team_id' => 'required|exists:clubs,id',
            'home_team_score' => 'required|integer',
            'away_team_score' => 'required|integer',
        ]);

        $matchScore->update($validatedData);

        return response()->json($matchScore);
    }

    public function destroy($id)
    {
        $matchScore = MatchScore::findOrFail($id);
        $matchScore->delete();
        return response()->json(null, 204);
    }

    public function standing()
    {
        $matchScores = MatchScore::all();
        $clubs = Club::all();
        $klasemen = [];

        foreach ($clubs as $club) {
            $ma = 0;
            $me = 0;
            $s = 0;
            $k = 0;
            $gm = 0;
            $gk = 0;
            $point = 0;

            foreach ($matchScores as $matchScore) {
                if ($matchScore->home_team_id == $club->id) {
                    $ma++;
                    $gm += $matchScore->home_team_score;
                    $gk += $matchScore->away_team_score;

                    if ($matchScore->home_team_score > $matchScore->away_team_score) {
                        $me++;
                        $point += 3;
                    } elseif ($matchScore->home_team_score == $matchScore->away_team_score) {
                        $s++;
                        $point += 1;
                    } else {
                        $k++;
                    }
                }

                if ($matchScore->away_team_id == $club->id) {
                    $ma++;
                    $gm += $matchScore->away_team_score;
                    $gk += $matchScore->home_team_score;

                    if ($matchScore->away_team_score > $matchScore->home_team_score) {
                        $me++;
                        $point += 3;
                    } elseif ($matchScore->away_team_score == $matchScore->home_team_score) {
                        $s++;
                        $point += 1;
                    } else {
                        $k++;
                    }
                }
            }

            $klasemen[] = [
                'club' => $club->name,
                'ma' => $ma,
                'me' => $me,
                's' => $s,
                'k' => $k,
                'gm' => $gm,
                'gk' => $gk,
                'point' => $point,
            ];
        }

        // Urutkan klasemen berdasarkan point, kemudian berdasarkan selisih gol
        usort($klasemen, function ($a, $b) {
            if ($a['point'] != $b['point']) {
                return $b['point'] - $a['point'];
            } else {
                return ($b['gm'] - $b['gk']) - ($a['gm'] - $a['gk']);
            }
        });

        return response()->json($klasemen);
    }
}
