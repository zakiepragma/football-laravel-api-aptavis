<?php

namespace App\Http\Controllers;

use App\Models\Club;
use Illuminate\Http\Request;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Club::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:clubs,name',
            'city' => 'required',
        ]);

        $clubs = Club::create($request->all());

        return response()->json(['data' => $clubs], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Club $club)
    {
        return response()->json(['data' => $club]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Club $club)
    {
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Club $club)
    {
        $request->validate([
            'name' => 'required|unique:clubs,name,' . $club->id,
            'city' => 'required',
        ]);

        $club->update($request->all());

        return response()->json(['data' => $club]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Club $club)
    {
        $club->delete();

        return response()->json(['data berhasil di hapus'], 200);
    }
}
